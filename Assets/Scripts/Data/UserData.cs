﻿//  UserData.cs
//  ProductName Template
//
//  Created by kan.kikuchi on 2016.04.28.

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 永続的に保存するデータを管理するクラス
/// </summary>
public static class UserData
{

    //=================================================================================
    //外部からの保存用
    //=================================================================================

    /// <summary>
    /// 諸々のデータを保存
    /// </summary>
    public static void Save()
    {
        PlayerPrefsUtility.Save(PLAY_COUNT_KEY, PlayCount);
        PlayerPrefsUtility.Save(IS_MUTE_KEY, IsMute);

        if (_isVibrate)
            PlayerPrefsUtility.Save(VIBRATE, 1);
        else
            PlayerPrefsUtility.Save(VIBRATE, 0);

        //全てのデータを同期
        PlayerPrefs.Save();
        Debug.Log("Save PlayerPrefs");
    }

    //=================================================================================
    //プレイカウント
    //=================================================================================
    private const string PLAY_COUNT_KEY = "PLAY_COUNT_KEY";

    private static bool _isInitializedPlayCount = false;
    private static int _playCount = 0;

    /// <summary>
    /// ゲームをプレイした回数
    /// </summary>
    public static int PlayCount
    {
        get
        {
            if (!_isInitializedPlayCount)
            {
                _playCount = PlayerPrefsUtility.Load(PLAY_COUNT_KEY, _playCount);
                _isInitializedPlayCount = true;
            }
            return _playCount;
        }
        set { _playCount = value; }
    }

    //=================================================================================
    //ミュートかどうか
    //=================================================================================
    private const string IS_MUTE_KEY = "IS_MUTE_KEY";

    private static bool _isInitializedMuteSetting = false;
    private static bool _isMute = false;

    //ミュートの状態が変わった時のイベント
    public static event Action<bool> OnChangeMuteSetting = delegate { };

    /// <summary>
    /// ミュートに設定されてるか否か
    /// </summary>
    public static bool IsMute
    {
        get
        {
            if (!_isInitializedMuteSetting)
            {
                _isMute = PlayerPrefsUtility.Load(IS_MUTE_KEY, _isMute);
                _isInitializedMuteSetting = true;
            }
            return _isMute;
        }
        set
        {
            _isMute = value;
            OnChangeMuteSetting(_isMute);
        }
    }

    private static bool _isInitializedVibrate = false;
    private static bool _isVibrate;

    private const string VIBRATE = "VIBRATE_TEMPLATE";

    public static bool IsVibrate
    {
        get
        {
            if (!_isInitializedVibrate)
            {
                Debug.Log("Model : " + SystemInfo.deviceModel);

                if (SystemInfo.deviceModel.Contains("iPad"))
                    _isVibrate = PlayerPrefs.GetInt(VIBRATE, 0) == 1;
                else
                    _isVibrate = PlayerPrefs.GetInt(VIBRATE, 1) == 1;

                _isInitializedVibrate = true;
            }

            return _isVibrate;
        }
        set
        {
            _isVibrate = value;
        }
    }
}
