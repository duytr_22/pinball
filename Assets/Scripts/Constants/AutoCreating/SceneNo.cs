﻿/// <summary>
/// シーン番号を定数で管理するクラス
/// </summary>
public static class SceneNo{

	  public const int GAMEPLAY     = 0;
	  public const int SAMPLE_SCENE = 1;
	  public const int TITLE        = 2;

}
