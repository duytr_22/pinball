﻿/// <summary>
/// レイヤー番号を定数で管理するクラス
/// </summary>
public static class LayerNo{

	  public const int BACK           = 8;
	  public const int BALL           = 13;
	  public const int DEFAULT        = 0;
	  public const int EFFECT         = 9;
	  public const int FLIPPER        = 12;
	  public const int GROUND         = 11;
	  public const int IGNORE_RAYCAST = 2;
	  public const int RAG_DOLL       = 10;
	  public const int TRANSPARENT_FX = 1;
	  public const int UI             = 5;
	  public const int WATER          = 4;

}
