﻿/// <summary>
/// シーン名を定数で管理するクラス
/// </summary>
public static class SceneName{

	  public const string GAMEPLAY     = "Gameplay";
	  public const string SAMPLE_SCENE = "SampleScene";
	  public const string TITLE        = "Title";

}
