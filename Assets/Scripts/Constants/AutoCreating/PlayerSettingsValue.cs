﻿/// <summary>
/// PlayerSettingsの設定を定数で管理するクラス
/// </summary>
public static class PlayerSettingsValue{

	  public const string BUNDLE_IDENTIFIER = "jp.aiayatsuji.pinball";
	  public const string BUNDLE_VERSION    = "0.1";
	  public const string PRODUCT_NAME      = "pinball";

}
