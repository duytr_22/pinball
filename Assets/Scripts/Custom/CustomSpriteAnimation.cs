﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomSpriteAnimation : MonoBehaviour
{
    public string NameFormat = "{0}";
    public float Rate = 0.1f;
    public int Min, Max;
    public bool Loop = false;

    private UISprite _sprite;
    private int _idx = 0;

    // Start is called before the first frame update
    void Start()
    {
        _sprite = GetComponent<UISprite>();

        _idx = Min;

        if (Loop)
            Invoke("UpdateSprite", Rate);
        else
            Invoker.InvokeDelayed(UpdateSprite, Rate);
    }

    // Update is called once per frame
    void UpdateSprite()
    {
        _idx++;

        if (_idx > Max)
        {
            if(Loop)
                _idx = Min;
            else
                _idx = Max;
        }

        _sprite.spriteName = string.Format(NameFormat, _idx);

        if(_idx <= Max)
        {
            Invoke("UpdateSprite", Rate);
        }
    }
}
