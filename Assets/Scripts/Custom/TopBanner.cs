﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopBanner : MonoBehaviour
{
    private UISprite _sprite;

    private void Start()
    {
        _sprite = GetComponent<UISprite>();

        float screenRate = (float)Screen.height / (float)Screen.width;

        if (screenRate >= 2.0f)
        {
            _sprite.SetAnchor((Transform)null);

            transform.localPosition += Vector3.down * 50;
            // #if UNITY_ANDROID
            //             _sprite.SetAnchor(_root, 0, 1504, 0, 50);
            // #else
            //             _sprite.SetAnchor(_root, 0, 1554, 0, 50);
            // #endif

        }
    }
}
