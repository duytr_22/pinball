using System.Runtime.InteropServices;
using UnityEngine;

// 'Peek' feedback (weak boom) - 1519
// 'Pop' feedback (strong boom) - 1520
// 'Cancelled' feedback (three sequential weak booms) - 1521
// 'Success' feedback (week boom then strong boom) - 1102
// 'Failed' feedback (three sequential strong booms) - 1107

public enum EVibrationType
{
    VibrateSuperLight_Peek,
    VibrateLight_Pop,
    VibrateLightLong_Cancelled,
    Success,
    Failed,
}


public static class CustomVibration
{
    public static void Do(EVibrationType type, bool fallback = false)
    {
        if (!UserData.IsVibrate)
        {
            Debug.Log("NO VIBRATE !!!");
            return;
        }

#if UNITY_IOS && !UNITY_EDITOR
        UnityEngine.iOS.DeviceGeneration generation = UnityEngine.iOS.Device.generation;
        if(generation != UnityEngine.iOS.DeviceGeneration.iPhone
            && generation != UnityEngine.iOS.DeviceGeneration.iPhone3G
            && generation != UnityEngine.iOS.DeviceGeneration.iPhone3GS
            && generation != UnityEngine.iOS.DeviceGeneration.iPhone4
            && generation != UnityEngine.iOS.DeviceGeneration.iPhone4S
            && generation != UnityEngine.iOS.DeviceGeneration.iPhone5
            && generation != UnityEngine.iOS.DeviceGeneration.iPhone5C  
            && generation != UnityEngine.iOS.DeviceGeneration.iPhone5S
            && generation != UnityEngine.iOS.DeviceGeneration.iPhone6
            && generation != UnityEngine.iOS.DeviceGeneration.iPhone6Plus)
        {
            _UnityCustomVibrationDo((int)type);
        }
        else if(fallback)
        {
            Handheld.Vibrate();
        }
#elif UNITY_ANDROID && !UNITY_EDITOR
        return;
		CommonNativeManager.Instance.CommonAndroidPlugin.Call("vibrateSuperLight");
#endif
    }

    public static void Vibrate()
    {
        if (UserData.IsVibrate)
        {
            Handheld.Vibrate();
        }
    }

#if UNITY_IOS && !UNITY_EDITOR
    [DllImport("__Internal")]
    private static extern void _UnityCustomVibrationDo(int type);
#elif UNITY_ANDROID && !UNITY_EDITOR

#endif
}