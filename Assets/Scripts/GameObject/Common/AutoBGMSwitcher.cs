﻿//  AutoBGMSwitcher.cs
//  ProductName Template
//
//  Created by kan.kikuchi on 2016.05.09.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// BGMを自動で切り替えるクラス
/// </summary>
public class AutoBGMSwitcher : MonoBehaviour
{

    private void Start()
    {
        //BGM設定(ミュート設定後に再生したいので、1フレーム後に)
        StartCoroutine(this.DelayMethod(0.01f, () =>
        {
            ChangeBGM(SceneNavigator.Instance.CurrentSceneNo);
        }));

        //シーン移動後にBGMを変えるように
        SceneNavigator.Instance.OnLoadedScene += ChangeBGM;
    }

    private void ChangeBGM(int sceneNo)
    {
        BGMManager.Instance.PlayBGM(AudioName.BGM_IN_GAME);
    }

}