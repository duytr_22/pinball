﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResumeButtonAssistant : ButtonAssistant
{
    public GameObject Popup;

    //クリックした時に実行されるメソッド
    protected override void OnClick()
    {
        base.OnClick();
        ChangeState(isPause: false);
    }

    private void ChangeState(bool isPause)
    {
        Time.timeScale = isPause ? 0 : 1;

        Popup.SetActive(isPause);

        if (isPause)
        {
            BGMManager.Instance.Pause();
        }
        else
        {
            BGMManager.Instance.Resume();
        }
    }
}
