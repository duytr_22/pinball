﻿//  ButtonAssistant.cs
//  ProductName Template
//
//  Created by kan.kikuchi on 2016.04.18.

using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

[RequireComponent (typeof(UIButton))]

/// <summary>
/// ボタンの補助をするクラス
/// </summary>
public class ButtonAssistant : MonoBehaviour
{

	protected UIButton _button;

	//クリックした時のイベント
	public event Action onClick = delegate {};

	//=================================================================================
	//初期化
	//=================================================================================

	protected virtual void Awake ()
	{
		//ボタンを取得
		_button = GetComponent<UIButton> ();

//		EventDelegate.Set (_button.onClick, delegate() {
//			OnClick ();
//		});
	}

	//=================================================================================
	//内部
	//=================================================================================

	//クリックした時に実行されるメソッド
	protected virtual void OnClick ()
	{
		SEManager.Instance.PlaySE (AudioName.SE_BUTTON);
		onClick ();
	}

}