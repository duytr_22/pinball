﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RewardButtonAssistant : ButtonAssistant
{
    private void Start()
    {
        EnableMe(AdManager.Instance.CheckCanShowVideo());
    }
    private void OnEnable()
    {

        AdManager.RewardVideoLoaded += OnVideoLoaded;
    }
    private void OnDisable()
    {
        AdManager.RewardVideoComplete -= AdManager_RewardVideoComplete;
        AdManager.RewardVideoLoaded -= OnVideoLoaded;
    }

    void OnVideoLoaded()
    {
        EnableMe(true);
    }

    void EnableMe(bool isEnable)
    {
        _button.isEnabled = isEnable;
    }

    protected void ShowAdVideo()
    {
        AdManager.RewardVideoComplete += AdManager_RewardVideoComplete;
#if UNITY_EDITOR || CHEAT
        AdManager.Instance.CheatReward();
#else
        AdManager.Instance.ShowRewardVideo();
#endif
        EnableMe(false);
    }
    void AdManager_RewardVideoComplete(bool isSuccess)
    {
        OnVideoSuccess(isSuccess);
        EnableMe(AdManager.Instance.CheckCanShowVideo());
        AdManager.RewardVideoComplete -= AdManager_RewardVideoComplete;
    }
    protected virtual void OnVideoSuccess(bool isSuccess)
    {

    }
}

