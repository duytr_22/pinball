﻿//  CommonNativeManager.cs
//  ProductName 激ムズ!タワー崩し
//
//  Created by kan kikuchi on 2015.10.27.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// ネイティブ関連の汎用的なマネージャ
/// </summary>
public class CommonNativeManager : SingletonMonoBehaviour<CommonNativeManager>
{

    //ネイティブ連携用プラグイン
    private CommonNativePlugin _commonNativePlugin;
#if UNITY_ANDROID
	public  AndroidJavaObject  CommonAndroidPlugin{
		get{return _commonNativePlugin.AndroidPlugin;}
	}
#endif

    //=================================================================================
    //初期化
    //=================================================================================

    protected override void Init()
    {
        base.Init();

        //ネイティブ連携用プラグイン初期化
        _commonNativePlugin = gameObject.AddComponent<CommonNativePlugin>();

#if !NO_NAITIVE
        _commonNativePlugin.Init();
#endif

    }

    /// <summary>
    /// アプリ内でAppStoreを開く
    /// </summary>
    public void ShowAppStoreInApp(int id)
    {
#if !NO_NAITIVE
        _commonNativePlugin.ShowAppStoreInApp(id);
#endif
    }

    /// <summary>
    /// レビュー催促を表示する
    /// </summary>
    public void ShowReviewPopUp()
    {
#if !NO_NAITIVE
        _commonNativePlugin.ShowReviewPopUp();
#endif
    }

    public void ReportAnalytic(string key)
    {
        Debug.Log("Report Analytic: " + key);
#if !NO_NAITIVE
        _commonNativePlugin.ReportToFacebookAnalytic(key);
        _commonNativePlugin.ReportToFlurry(key);
#endif

    }
}