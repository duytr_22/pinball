﻿//  ShareManager.cs
//  ProductName BreakoutTower
//
//  Created by kan kikuchi on 2015.10.26.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// シェア関連のマネージャ
/// </summary>
public class ShareManager : SingletonMonoBehaviour<ShareManager>
{

    //ネイティブ連携用プラグイン
#if !NO_NAITIVE
    private SharePlugin _sharePlugin;
#endif

    //=================================================================================
    //初期化
    //=================================================================================

    protected override void Init()
    {
        base.Init();

        //ネイティブ連携用プラグイン初期化
#if !NO_NAITIVE
        _sharePlugin = gameObject.AddComponent<SharePlugin>();
        _sharePlugin.Init();
#endif
    }

    //=================================================================================
    //外部
    //=================================================================================

    /// <summary>
    /// Tweet
    /// </summary>
    public void Tweet(string shareMessage)
    {

#if !NO_NAITIVE

        string path = "";

        {
            ScreenCapture.CaptureScreenshot("/Screenshot.png");
            path = Application.persistentDataPath + "/Screenshot.png";
        }

        //シェア
        _sharePlugin.Tweet(shareMessage, path);
#endif
    }

}
