﻿//  RankingPlugin.cs
//  ProductName BreakoutTower
//
//  Created by kan kikuchi on 2015.10.26.

//モバイルの時だけMOBILE_DEVICEを宣言する
#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBGL || UNITY_WEBPLAYER
#undef MOBILE_DEVICE
#else
#define MOBILE_DEVICE
#endif

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

/// <summary>
/// ランキング関連のネイティブプラグイン
/// </summary>
public class RankingPlugin : NativePlugin {

	//=================================================================================
	//初期化
	//=================================================================================

	/// <summary>
	/// 初期化
	/// </summary>
	public void Init(){

		#if UNITY_IOS
		base.Init (_RankingPlugin_Init, "");
		#elif UNITY_ANDROID
		base.Init (null, "");
		#endif
	}

	#if UNITY_IOS
	[DllImport("__Internal")]
	private static extern IntPtr _RankingPlugin_Init(string gameObjectName);
	#endif

	//=================================================================================
	//リーダーボード表示
	//=================================================================================

	/// <summary>
	/// リーダーボード表示
	/// </summary>
	public void ShowLeaderBoard(){
		Debug.Log ("ShowLeaderBoard");	

		#if UNITY_IOS && MOBILE_DEVICE
		if (_nativeInstance == IntPtr.Zero){
			return;
		}
		_ShowLeaderBoard(_nativeInstance);
		#elif UNITY_ANDROID && MOBILE_DEVICE
		CommonNativeManager.Instance.CommonAndroidPlugin.Call("showLeaderBoard", 0);
		#endif
	}

	#if UNITY_IOS
	[DllImport("__Internal")]
	private static extern void  _ShowLeaderBoard (IntPtr rankingInstance);
	#endif

	//=================================================================================
	//スコア送信
	//=================================================================================

	/// <summary>
	/// スコアをリーダーボードに送信
	/// </summary>
		public void ReportScore(int rankingNo, int currentScore){
		Debug.Log (rankingNo + ", スコア:" + currentScore);

		#if UNITY_IOS && MOBILE_DEVICE
		if (_nativeInstance == IntPtr.Zero){
			return;
		}
		_RankingReportIntScore(_nativeInstance, rankingNo, currentScore);	
		#elif UNITY_ANDROID && MOBILE_DEVICE
		CommonNativeManager.Instance.CommonAndroidPlugin.Call("reportScore", rankingNo, currentScore);
		#endif	
	}

	#if UNITY_IOS
	[DllImport("__Internal")]
	private static extern void  _RankingReportIntScore (IntPtr instance, int rankingNo, int currentScore);
	#endif

	/// <summary>
	/// スコアをリーダーボードに送信
	/// </summary>
	public void ReportScore(int rankingNo, float currentScore){
		Debug.Log (rankingNo + ", スコア:" + currentScore);

		#if UNITY_IOS && MOBILE_DEVICE
		if (_nativeInstance == IntPtr.Zero){
			return;
		}
		_RankingReportFloatScore(_nativeInstance, rankingNo, currentScore);	
		#elif UNITY_ANDROID && MOBILE_DEVICE
		CommonNativeManager.Instance.CommonAndroidPlugin.Call("reportScore", rankingNo, (int)(currentScore));
		#endif
	}

	#if UNITY_IOS
	[DllImport("__Internal")]
	private static extern void  _RankingReportFloatScore (IntPtr instance, int rankingNo, float currentScore);
	#endif

}