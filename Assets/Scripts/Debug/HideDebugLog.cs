﻿//  Debug.cs
//  ProductName 激ムズ!タワー崩し
//
//  Created by kan kikuchi on 2015.11.02.

using System;
using UnityEngine;

//SHOW_DEBUGが宣言されていない時はDebugの全関数を無効にする
#if !SHOW_DEBUG
public static class Debug{
	public static void Assert(bool condition){}
	public static void Assert(bool condition, string message){}
	public static void Assert(bool condition, string format, params object[] args){}

	static public void Break(){}

	public static void ClearDeveloperConsole(){}

	public static void DrawLine(Vector3 start, Vector3 end){}
	public static void DrawLine(Vector3 start, Vector3 end, Color color){}
	public static void DrawLine(Vector3 start, Vector3 end, Color color, float duration){}
	public static void DrawLine(Vector3 start, Vector3 end, Color color, float duration, bool depthTest){}

	static public void DrawRay (Vector3 start, Vector3 dir, Color color){}
	static public void DrawRay (Vector3 start, Vector3 dir, Color color, float duration){}
	static public void DrawRay (Vector3 start, Vector3 dir, Color color, float duration, bool depthTest){}

	static public void Log(object message, UnityEngine.Object context = null){}
	public static void LogFormat(string format, params object[] args){}
	public static void LogFormat(UnityEngine.Object context, string format, params object[] args){}

	static public void LogError(object message, UnityEngine.Object context = null){}
	public static void LogErrorFormat(string format, params object[] args){}
	public static void LogErrorFormat(UnityEngine.Object context, string format, params object[] args){}

	public static void LogException(Exception exception){}
	public static void LogException(Exception exception, UnityEngine.Object context){}

	static public void LogWarning(object message, UnityEngine.Object context = null){}
	public static void LogWarningFormat(string format, params object[] args){}
	public static void LogWarningFormat(UnityEngine.Object context, string format, params object[] args){}

	static public void DrawRay (Vector3 start, Vector3 dir){}
}
#endif