﻿//  FPSTest.cs
//  ProductName EraserCrashers
//
//  Created by kikuchikan on 2015.07.08.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// FPSを表示するテストスクリプト
/// </summary>
public class FPSUI : TestUI {

	private List<float> _fpsList = new List<float>();
	private const float FPS_COUNT = 20;

	private float _fps = 60;

	//=================================================================================
	//初期化
	//=================================================================================

	protected override void Awake(){
		base.Awake ();

		//ラベル色を青に
		GUIStyleState styleState = new GUIStyleState();
		styleState.textColor = Color.blue;
		_labelStyle.normal = styleState;

	}

	//=================================================================================
	//内部
	//=================================================================================

	//FPS計算
	private void CalcFPS(){

		//フレーム毎のFPSを貯める
		_fpsList.Add (1.0f / Time.unscaledDeltaTime);
		if(_fpsList.Count < FPS_COUNT){
			return;
		}

		//一定以上貯まったら平均を出し、最初から貯める
		_fps = 0;
		foreach (float fps in _fpsList) {
			_fps += fps;
		}
		_fps /= _fpsList.Count;

		_fpsList.Clear ();
	}

	//=================================================================================
	//GUI表示
	//=================================================================================

	/// <summary>
	/// 継承先でGUIの処理を書く
	/// </summary>
	protected override void UpdateGUI(){
		//FPS計算
		CalcFPS ();

		//ラベル表示
		GUI.Label(
			new Rect(0, SCREEN_SIZE.y - 100, 300, 50), 
			"FPS : " + _fps.ToString("F2"), 
			_labelStyle
		);
	}

}