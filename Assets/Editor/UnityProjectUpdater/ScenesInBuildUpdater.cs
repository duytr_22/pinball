﻿//  SceneSetter.cs
//  ProductName Template
//
//  Created by kan.kikuchi on 2016.04.22.

using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// シーンを自動でビルドに設定するクラス
/// </summary>
public class ScenesInBuildUpdater : AssetPostprocessorEx
{

    private const string STAGE_KEY = "Stage";

    //returnの値が小さいほど優先される
    public override int GetPostprocessOrder()
    {
        return -5;
    }

    //=================================================================================
    //検知
    //=================================================================================

    // private static void OnPostprocessAllAssets (string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
    // {
    // 	//最初の起動時に実行しないように
    // 	if (!UnityProjectSetting.IsLaunchedProject) {
    // 		return;
    // 	}

    // 	List<string[]> assetsList = new List<string[]> () {
    // 		importedAssets, deletedAssets, movedAssets, movedFromAssetPaths
    // 	};

    // 	List<string> targetDirectoryNameList = new List<string> () {
    // 		DirectoryPath.TOP_RESOURCES + ResourcesDirectoryPath.DATA,
    // 		DirectoryPath.SCENES,
    // 	};

    // 	if (ExistsDirectoryInAssets (assetsList, targetDirectoryNameList)) {
    // 		UpdateScenesInBuild ();
    // 	}

    // }

    //=================================================================================
    //作成
    //=================================================================================

    [MenuItem("Tools/Update/Reset Data")]
    private static void ResetData()
    {

        PlayerPrefs.DeleteAll();
        PlayerPrefs.Save();
    }

    //スクリプトを作成します
    [MenuItem("Tools/Update/Scenes In Build")]
    private static void UpdateScenesInBuild()
    {
        List<string> sceneNameList = new List<string>();

        foreach (var guid in AssetDatabase.FindAssets("t:Scene"))
        {

            var path = AssetDatabase.GUIDToAssetPath(guid);

            string sceneName = System.IO.Path.GetFileNameWithoutExtension(path);

            //シーン名が被っている
            if (sceneNameList.Contains(path))
            {

                Debug.LogError(sceneName + " scene is duplicated");

            }
            else
            {

                if (sceneName.Equals(SceneName.TITLE))
                {

                    sceneNameList.Insert(0, path);

                }
                else
                {

                    sceneNameList.Add(path);
                }
            }
        }

        List<EditorBuildSettingsScene> sceneList = new List<EditorBuildSettingsScene>();

        foreach (string sceneName in sceneNameList)
        {
            sceneList.Add(new EditorBuildSettingsScene(sceneName, true));
        }

        EditorBuildSettings.scenes = sceneList.ToArray();
    }

}