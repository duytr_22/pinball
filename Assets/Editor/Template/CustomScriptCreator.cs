﻿using System;
using System.IO;
using System.Text;
using UnityEditor;
using UnityEngine;

/// <summary>
/// テンプレートから新しくスクリプトを作るクラス
/// </summary>
public class CustomScriptCreator : EditorWindow {

	//メニューのパス
	protected const string MENU_PATH = "Assets/Create/Custom Script/";

	//作成する元のテンプレート名
	private static string _templateScriptName = "";

	//新しく作成するスクリプト及びクラス名
	private static string _newScriptName = "";
	//スクリプトの説明文
	private static string _scriptSummary = "";
	//作成者名
	private static string _authorName = "";
	//作成日
	private static string _createdData = "";

	//=================================================================================
	//ウィンドウ表示
	//=================================================================================

	protected static void ShowWindow(string templateScriptName){
		//各項目を初期化
		_templateScriptName = templateScriptName;
		_newScriptName      = templateScriptName;
		_createdData        = DateTime.Now.ToString ("yyyy.MM.dd");

		//作成者名は既に設定されてある場合は初期化しない
		_authorName = DeveloperInfo.GetDeveloperName();

		//ウィンドウ作成
		EditorWindow.GetWindow<CustomScriptCreator>("Create Script"); 
	}

	//表示するGUI設定
	private void OnGUI(){

		//作成日と元テンプレートを表示
		EditorGUILayout.LabelField("Template Script Name : " + _templateScriptName);
		GUILayout.Space(0);
		EditorGUILayout.LabelField("Created Data : " + _createdData);
		GUILayout.Space(10);

		//新しく作成するスクリプト及びクラス名の入力欄
		GUILayout.Label("New Script Name");
		_newScriptName = GUILayout.TextField(_newScriptName);
		GUILayout.Space(10);

		//スクリプトの説明文
		GUILayout.Label("Script Summary");
		_scriptSummary = GUILayout.TextArea(_scriptSummary);
		GUILayout.Space(10);

		//作成者名の入力欄、変更されたら保存
		EditorGUI.BeginChangeCheck ();

		GUILayout.Label("Author Name");
		_authorName = GUILayout.TextField(_authorName);
		GUILayout.Space(30);

		if (EditorGUI.EndChangeCheck ()){
			DeveloperInfo.SetDeveloperName (_authorName);
		}

		//作成ボタン、作成が成功したらウィンドウを閉じる
		if(GUILayout.Button("Create") ){
			if(CreateScript()){
				this.Close ();
			}
		}

		//開いてる時にビルドするなどしてテンプレ名などが空になった場合、ウィンドウを閉じる
		if(string.IsNullOrEmpty(_templateScriptName)){
			this.Close ();
		}
	}

	//=================================================================================
	//スクリプト作成
	//=================================================================================

	private static bool CreateScript(){

		//スクリプト名が空欄の場合は作成失敗
		if(string.IsNullOrEmpty(_newScriptName)){
			Debug.Log ("スクリプト名が入力されていないため、スクリプトが作成できませんでした");
			return false;
		}

		//現在選択しているディレクトリのパスを取得、選択されていない場合はスクリプト作成失敗
		string directoryPath = AssetDatabase.GetAssetPath( Selection.activeObject );
		if(string.IsNullOrEmpty(directoryPath)){
			Debug.Log ("作成場所が選択されていないため、スクリプトが作成できませんでした");
			return false;
		}

		//同名ファイルがあった場合はスクリプト作成失敗にする(上書きしてしまうため)
		string exportPath = directoryPath + "/" + _newScriptName + ExtensionName.SCRIPT;

		if (System.IO.File.Exists(exportPath)){
			Debug.Log (exportPath + "が既に存在するため、スクリプトが作成できませんでした");
			return false;
		}

		//テンプレートへのパスを作成しテンプレート読み込み
		string templatePath = DirectoryPath.TEMPLATE_SCRIPT_DIRECTORY_PATH + "/" + _templateScriptName + ExtensionName.TEMPLATE_SCRIPT;
		StreamReader streamReader = new StreamReader(templatePath, Encoding.GetEncoding("Shift_JIS"));
		string scriptText = streamReader.ReadToEnd();

		//各項目を置換
		scriptText = scriptText.Replace (TemplateTag.PRODUCT_NAME_TAG, PlayerSettings.productName);
		scriptText = scriptText.Replace (TemplateTag.AUTHOR_TAG      , _authorName);
		scriptText = scriptText.Replace (TemplateTag.DATA_TAG        , _createdData);
		scriptText = scriptText.Replace (TemplateTag.SUMMARY_TAG     , _scriptSummary.Replace("\n", "\n/// ")); //改行するとコメントアウトから外れるので///を追加
		scriptText = scriptText.Replace (TemplateTag.SCRIPT_NAME_TAG , _newScriptName);

		//スクリプトを書き出し
		File.WriteAllText (exportPath, scriptText, Encoding.UTF8);
		AssetDatabase.Refresh (ImportAssetOptions.ImportRecursive);

		return true;
	}

}