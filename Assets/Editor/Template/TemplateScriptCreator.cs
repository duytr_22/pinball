﻿using System;
using System.IO;
using System.Text;
using UnityEditor;
using UnityEngine;

/// <summary>
/// 選択しているスクリプトからテンプレートを作るクラス
/// </summary>
public class TemplateScriptCreator : EditorWindow {

	//テンプレート上部に差し込む上部定型テキスト
	private const string TOP_TEXT = 
		"//  " + TemplateTag.SCRIPT_NAME_TAG + ExtensionName.SCRIPT+ "\n" +
		"//  ProductName " + TemplateTag.PRODUCT_NAME_TAG + "\n" +
		"//\n" +
		"//  Created by " + TemplateTag.AUTHOR_TAG + " on " + TemplateTag.DATA_TAG + "\n";

	//クラス説明のサマリー用定型テキスト Between
	private const string CLASS_SUMMARY_BETWEEN = "/// "+ TemplateTag.SUMMARY_TAG + "\n";
	private const string CLASS_SUMMARY = 
		"\n/// <summary>\n" +
		CLASS_SUMMARY_BETWEEN +
		"/// </summary>";

	//メニューのパス
	private const string MENU_PATH = "Assets/Create/Template Script";

	//元スクリプトへのパス
	private static string _originalScriptPath = "";

	//テンプレート名
	private static string _templateName = "";

	//=================================================================================
	//ウィンドウ表示
	//=================================================================================

	[MenuItem(MENU_PATH)]
	private static void ShowWindow(){

		//現在選択しているディレクトリのパスを取得
		_originalScriptPath = AssetDatabase.GetAssetPath(Selection.activeObject);
		if(string.IsNullOrEmpty(_originalScriptPath)){
			Debug.Log ("テンプレートの元ファイルが選択されていないため、テンプレートが作成できません");
			return;
		}

		//拡張子を取得し、スクリプトファイルか判定
		string extension = new System.IO.FileInfo(_originalScriptPath).Extension;
		if(extension != ExtensionName.SCRIPT){
			Debug.Log ("拡張子が " + ExtensionName.SCRIPT + "のファイルが選択されていないため、テンプレートが作成できません");
			return;
		}

		//作成するテンプレート名に選択したスクリプト名を仮設定
		_templateName = System.IO.Path.GetFileNameWithoutExtension(_originalScriptPath);

		//ウィンドウ作成
		EditorWindow.GetWindow<TemplateScriptCreator>("Create Template"); 
	}

	//表示するGUI設定
	private void OnGUI()
	{

		//元スクリプトのパスを表示
		EditorGUILayout.LabelField("Original Script : " + _originalScriptPath);
		GUILayout.Space(10);

		//新しく作成するテンプレ名の入力欄
		GUILayout.Label("New Template Name");
		_templateName = GUILayout.TextField(_templateName);
		GUILayout.Space(10);

		//作成ボタン、作成が成功したらウィンドウを閉じる
		if(GUILayout.Button("Create") ){
			if(CreateTemplate()){
				this.Close ();
			}
		}

	}

	//=================================================================================
	//スクリプト作成
	//=================================================================================

	private static bool CreateTemplate(){

		//テンプレート名が空欄の場合は作成失敗
		if(string.IsNullOrEmpty(_templateName)){
			Debug.Log ("テンプレート名が入力されていないため、テンプレートが作成できませんでした");
			return false;
		}

		//同名ファイルがあった場合はテンプレート作成失敗にする(上書きしてしまうため)
		string exportPath = DirectoryPath.TEMPLATE_SCRIPT_DIRECTORY_PATH + "/" + _templateName + ExtensionName.TEMPLATE_SCRIPT;

		if (System.IO.File.Exists(exportPath)){
			Debug.Log (exportPath + "が既に存在するため、テンプレートが作成できませんでした");
			return false;
		}

		//元スクリプト読み込み
		StreamReader streamReader = new StreamReader(_originalScriptPath, Encoding.GetEncoding("Shift_JIS"));
		string scriptText = streamReader.ReadToEnd();

		//クラス名をタグ置換
		scriptText = ReplaceClassNameToTag (scriptText);

		//クラス説明のSummaryを置換
		scriptText = ReplaceClassSummaryToTag (scriptText);

		//元スクリプトがテンプレートから作った時などに上部定型テキストがあるかもしれないので、最初のusingより上を削除
		int usingFirstPoint = scriptText.IndexOf ("using");
		if(usingFirstPoint > 0){
			scriptText = scriptText.Remove (0, usingFirstPoint - 1);
		}
		else{
			scriptText = "\n" + scriptText;
		}

		//上部定型テキスト差し込み
		scriptText = TOP_TEXT + scriptText;

		//スクリプトを書き出し
		File.WriteAllText (exportPath, scriptText, Encoding.UTF8);
		AssetDatabase.Refresh (ImportAssetOptions.ImportRecursive);

		return true;
	}

	//クラス説明のSummaryを置換
	private static string ReplaceClassSummaryToTag(string scriptText){

		int summaryFirstPoint       = scriptText.IndexOf ("summary");
		int scriptNameTagFirstPoint = scriptText.IndexOf (TemplateTag.SCRIPT_NAME_TAG);

		//クラス説明のSummaryが無い場合は、Summaryの定型を差し込む
		if(summaryFirstPoint == -1 || summaryFirstPoint > scriptNameTagFirstPoint){

			//"class "前の改行位置を調べる
			string classText = "class ";
			int classNameStartPoint = scriptText.IndexOf (classText);

			int lineBreakPoint = scriptText.IndexOf ("\n");
			int lineBreakPointBeforeClassName = 0;

			while(lineBreakPoint < classNameStartPoint){
				lineBreakPointBeforeClassName = lineBreakPoint;
				lineBreakPoint = scriptText.IndexOf ("\n", lineBreakPointBeforeClassName + 1);
			}

			//"class "前の改行のさらに前にSummaryの定型を差し込む
			scriptText = scriptText.Insert (lineBreakPointBeforeClassName, CLASS_SUMMARY);

		}
		//クラス説明のSummaryがある場合は最初の<summary>の後から</summary>の前の改行まで全て削除し、Summaryの定型の間だけ差し込む
		else{
			string summaryStartText = "<summary>";
			int removeStartPoint = scriptText.IndexOf (summaryStartText) + summaryStartText.Length;

			string summaryEndText   = "</summary>";
			int summaryEndTextPoint = scriptText.IndexOf (summaryEndText);

			int lineBreakPoint = scriptText.IndexOf ("\n");
			int removeEndPoint = 0;

			while(lineBreakPoint < summaryEndTextPoint){
				removeEndPoint = lineBreakPoint;
				lineBreakPoint = scriptText.IndexOf ("\n", removeEndPoint + 1);
			}

			scriptText = scriptText.Remove (removeStartPoint, removeEndPoint - removeStartPoint);
			scriptText = scriptText.Insert (removeStartPoint + 1, CLASS_SUMMARY_BETWEEN);
		}

		return scriptText;
	}

	//クラス名をタグ置換
	private static string ReplaceClassNameToTag(string scriptText){

		//全置換すると関係ない所も置換するので、一旦クラス名を削除した後にタグを差し込む
		//クラス名で検索すると他の所が引っかかるかもしれないので"class "を使って検索する
		string classText = "class ";
		int classNameStartPoint = scriptText.IndexOf (classText) + classText.Length;
		string classname = System.IO.Path.GetFileNameWithoutExtension (_originalScriptPath);

		scriptText = scriptText.Remove (classNameStartPoint, classname.Length);
		scriptText = scriptText.Insert (classNameStartPoint, TemplateTag.SCRIPT_NAME_TAG);

		return scriptText;
	}

}
