﻿using UnityEditor;

/// <summary>
/// RecyclingObjectのテンプレートから新しくスクリプトを作るクラス
/// </summary>
public class RecyclingObjectScriptCreator : CustomScriptCreator {

	private const string TEMPLATE_SCRIPT_NAME  = "RecyclingObject";

	[MenuItem(MENU_PATH + TEMPLATE_SCRIPT_NAME)]
	private static void CreateScript(){
		ShowWindow (TEMPLATE_SCRIPT_NAME);
	}

}
