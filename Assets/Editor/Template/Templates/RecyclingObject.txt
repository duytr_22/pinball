﻿//  #SCRIPTNAME#.cs
//  ProductName #PRODUCTNAME#
//
//  Created by #AUTHOR# on #DATA#

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// #SUMMARY#
/// </summary>
public class #SCRIPTNAME# : RecyclingObject {

	//=================================================================================
	//初期化
	//=================================================================================

	/// <summary>
	/// 初期化
	/// </summary>
	public override void Init(){
		base.Init ();

	}

	/// <summary>
	/// 解放
	/// </summary>
	public override void Release(){
		base.Release ();

	}

}