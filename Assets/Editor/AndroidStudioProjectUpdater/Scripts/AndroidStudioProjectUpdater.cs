﻿//  AndroidStudioProjectUpdater.cs
//  ProductName Template
//
//  Created by Aaron on 2016.05.17.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.Callbacks;
using UnityEditor;
using System.IO;
using System.Xml;
using System.Text.RegularExpressions;



/// <summary>
/// Updates the Android Studio Project files overriding libraries and assets.
/// </summary>
public class AndroidStudioProjectUpdater : MonoBehaviour
{

    private static string FINAL_PATH = "AndroidProject/FinalProject";

    private static string MANIFEST_PATH = Path.Combine(FINAL_PATH, "app/src/main/AndroidManifest.xml");

    private static string GRADLE_CONFIG_PATH = Path.Combine(FINAL_PATH, "app/build.gradle");

    private static string[][] REPLACE_FILES_AND_DIRECTORIES = new string[][]{
        new string[] {"replace_file", "libs/unity-classes.jar", "libs/unity-classes.jar"},
        new string[] {"replace_directory", "src/main/assets", "src/main/assets"},
        new string[] {"replace_directory", "src/main/jniLibs", "src/main/jniLibs"}
    };

    private static string[] REPLACE_APP_NAME = new string[]{
        "app/src/main/res/values/strings.xml",
        "app/src/main/res/values-en/strings.xml",
        "app/src/main/res/values-ja/strings.xml"
    };

    [PostProcessBuildAttribute(1)]
    private static void OnPostprocessBuild(BuildTarget target, string pathToBuiltProject)
    {

        if (target != BuildTarget.Android)
        {
            return;
        }

        string sourcePath = Path.Combine(pathToBuiltProject, PlayerSettings.productName);

        Debug.Log("OnPostprocessBuild ANDROID <" + sourcePath + ">");

        Debug.Log("Copying and reaplacing libraries, assets and icons...");

        foreach (string[] r in REPLACE_FILES_AND_DIRECTORIES)
        {
            if (r[0] == "replace_file")
            {
                FileUtil.ReplaceFile(Path.Combine(sourcePath, r[1]), Path.Combine(FINAL_PATH, r[2]));
            }
            else if (r[0] == "replace_directory")
            {
                FileUtil.ReplaceDirectory(Path.Combine(sourcePath, r[1]), Path.Combine(FINAL_PATH, r[2]));
            }
        }

        FileUtil.DeleteFileOrDirectory(sourcePath);

        Debug.Log("OnPostprocessBuild END");
    }

}
