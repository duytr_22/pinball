﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flipper : MonoBehaviour
{
    public enum FlipperSide
    {
        Left, Right
    }
    public enum RotateState
    {
        NoRotate,
        OnRotate,
        RotateEnded
    }

    public FlipperSide typeOfSide;
    public RotateState state;

    [SerializeField] AnimationCurve _rotateSmoothCurve;
    float _timeCurve;
    Rigidbody _rigidBody;
    int _direction;
    Quaternion _startRotation;
    const float _maxRotateValue = 1200f;

    Vector3 _axis;

    void Awake()
    {
        _rigidBody = GetComponent<Rigidbody>();
    }

    void Start()
    {
        _direction = typeOfSide == FlipperSide.Left ? -1 : 1;
        _startRotation = _rigidBody.rotation;
        _axis = new Vector3(0, 0, -1);
    }


    void FixedUpdate()
    {
        switch (state)
        {
            case RotateState.OnRotate:
                // Debug.Log("on rotate");

                _timeCurve += Time.deltaTime;
                var smooth = _rotateSmoothCurve.Evaluate(_timeCurve);

                _rigidBody.AddForce(_axis * _maxRotateValue * smooth, ForceMode.Force);

                break;
            case RotateState.RotateEnded:
                // Debug.Log("rotate end");    

                _timeCurve = 0;

                break;
        }

        if (state == RotateState.RotateEnded)
        {
            //convert from 360 degree to 180 degree
            var offset = typeOfSide == FlipperSide.Right ? 0 : -360;

            // Debug.Log($"sub: {Mathf.Abs(transform.localRotation.eulerAngles.y + offset - _startRotation.eulerAngles.y)}");
            if (Mathf.Abs(transform.localRotation.eulerAngles.y + offset - _startRotation.eulerAngles.y) < 0.1f)
                state = RotateState.NoRotate;
        }
    }

    public void Rotate()
    {
        state = RotateState.OnRotate;
    }

    public void ReleaseRotate()
    {
        state = RotateState.RotateEnded;
    }
}
