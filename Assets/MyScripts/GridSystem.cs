﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class GridSystem : SingletonMonoBehaviour<GridSystem>
{
    struct GridCell
    {
        public Vector3 worldPosition;
        public Vector2Int gridPosition;

        public GridCell(Vector2Int gp, Vector3 wp) : this()
        {
            this.gridPosition = gp;
            this.worldPosition = wp;
        }
    }
    const int _row = 2;
    const int _column = 3;
    const float _size = 1;
    List<GridCell> _cellsList;

    protected override void Init()
    {
        _cellsList = new List<GridCell>();

        var _root = transform.position;
        for (int i = 0; i < _row; i++)
        {
            for (int j = 0; j < _column; j++)
            {
                var gp = new Vector2Int(i, j); //grid position
                var wp = new Vector3(
                    _root.x - j * _size,
                   _root.y,
                    _root.z + i * _size
                );

                GridCell cell = new GridCell(gp, wp);
                _cellsList.Add(cell);
            }
        }

        TestGrid();
    }

    public Vector3 GetWorldPositionByIndex(int i)
    {
        i = Mathf.Clamp(i, 0, _cellsList.Count);
        return _cellsList[i].worldPosition;
    }

    public Vector3 GetWorldPositionByRandomIndex()
    {
        var i = Random.Range(0, _cellsList.Count);
        return _cellsList[i].worldPosition;
    }

    void TestGrid()
    {
        for (int i = 0; i < _cellsList.Count; i++)
        {
            var obj = new GameObject("test cell");
            obj.transform.SetParent(transform);
            obj.transform.position = _cellsList[i].worldPosition;
        }
    }

    void OnDrawGizmos()
    {
        // Matrix4x4 rotationMatrix = Matrix4x4.TRS(transform.position, transform.rotation, transform.localScale);
        // Gizmos.matrix = transform.localToWorldMatrix;

        Gizmos.color = Color.red;

        var _root = transform.position;
        for (int i = 0; i < _row; i++)
        {
            for (int j = 0; j < _column; j++)
            {
                var gp = new Vector2Int(i, j); //grid position
                var wp = new Vector3(
                    _root.x - j * _size,
                    _root.y,
                    _root.z + i * _size
                );

                Gizmos.DrawWireCube(wp, new Vector3(_size, 0, _size));
            }
        }
    }

}
