﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugSystem : MonoBehaviour
{
	[SerializeField] List<UIButton> _subButtons, _addButtons;
	[SerializeField] UILabel _eSpeedLabel, _eTotalLabel, _ballsLabel, _spawnDelayLabel, _cameraLabel;
	[SerializeField] GameObject _debugContainer;
	[SerializeField] UISprite _debugButtonSprite;
	[SerializeField] UILabel _debugButtonLabel;
	[SerializeField] Transform _newCameraTransform;
	Vector3 _oldCameraPosition;
	Quaternion _oldCameraRotation;

	Camera _mainCamera;
	List<EventDelegate> _eventDelegate;
	const float _enemyMoveSpeedChange = 0.25f;
	const int _enemiesTotalChange = 1;
	const int _ballsChange = 1;
	const float _spawnDelayChange = 0.25f;
	string _eSpeedString = "E.Speed: {0}";
	string _eTotalString = "E.Total: {0}";
	string _ballString = "Balls: {0}";
	string _spawnDelayString = "Spawn Delay: {0}";
	static bool _isDebugOn = true;
	static int _cameraTransformID = 1;
	bool _isCameraDebugInit;

	public enum DebugType
	{
		EnemiesSpeed,
		EnemiesTotal,
		Balls,
		SpawnDelay
	}

	void Awake()
	{
		_mainCamera = Camera.main;
		if (_isCameraDebugInit == false)
		{
			_isCameraDebugInit = true;
			_oldCameraPosition = _mainCamera.transform.position;
			_oldCameraRotation = _mainCamera.transform.rotation;
		}

		UpdateDebugCamera();

		for (var i = 0; i < _subButtons.Count; i++)
		{
			var ev = new EventDelegate();

			ev.Set(this, "onSubButtonClick");
			ev.parameters[0].value = (DebugType) Enum.GetValues(typeof(DebugType)).GetValue(i);
			_subButtons[i].onClick.Add(ev);
		}

		for (var i = 0; i < _addButtons.Count; i++)
		{
			var ev = new EventDelegate();

			ev.Set(this, "onAddButtonClick");
			ev.parameters[0].value = (DebugType) Enum.GetValues(typeof(DebugType)).GetValue(i);
			_addButtons[i].onClick.Add(ev);
		}
	}

	void Start()
	{
		UpdateLabels();
	}

	public void onDebugOffButtonClick()
	{
		_isDebugOn = !_isDebugOn;
		_debugContainer.SetActive(_isDebugOn);
		// _debugButtonSprite.color = _isDebugOn ? Color.white : new Color(0, 0, 0, 1 / 255f);
		_debugButtonLabel.text = _isDebugOn ? "Debug Off" : "Debug On";
	}

	public void OnRestartButtonClick()
	{
		GameSystem.Instance.ReloadScene();
	}

	public void onSubButtonClick(DebugType type)
	{
		Debug.Log(type);

		switch (type)
		{
			case DebugType.EnemiesSpeed:
				Constants.EnemyMoveSpeed -= _enemyMoveSpeedChange;
				Debug.Log(Constants.EnemyMoveSpeed);
				break;
			case DebugType.EnemiesTotal:
				Constants.TotalEnemies -= _enemiesTotalChange;

				break;
			case DebugType.Balls:
				Constants.MaxBalls -= _ballsChange;

				break;
			case DebugType.SpawnDelay:
				Constants.SpawnDelay -= _spawnDelayChange;
				break;
		}

		UpdateLabels();
	}

	void UpdateLabels()
	{
		// Debug.Log(Constants.EnemyMoveSpeed);
		_eSpeedLabel.text = string.Format(_eSpeedString, Constants.EnemyMoveSpeed);
		_eTotalLabel.text = string.Format(_eTotalString, Constants.TotalEnemies);
		_ballsLabel.text = string.Format(_ballString, Constants.MaxBalls);
		_spawnDelayLabel.text = string.Format(_spawnDelayString, Constants.SpawnDelay);
		_cameraLabel.text = $"Camera: {(_cameraTransformID == 1 ? 45:65)}*";
	}

	public void onAddButtonClick(DebugType type)
	{
		Debug.Log(type);

		switch (type)
		{
			case DebugType.EnemiesSpeed:
				Constants.EnemyMoveSpeed += _enemyMoveSpeedChange;

				break;
			case DebugType.EnemiesTotal:
				Constants.TotalEnemies += _enemiesTotalChange;

				break;
			case DebugType.Balls:
				Constants.MaxBalls += _ballsChange;

				break;
			case DebugType.SpawnDelay:
				Constants.SpawnDelay += _spawnDelayChange;
				break;
		}

		UpdateLabels();
	}

	public void OnSwitchCameraClick()
	{
		_cameraTransformID *= -1;

		UpdateDebugCamera();
	}

	void UpdateDebugCamera()
	{
		var pos = _mainCamera.transform.position;
		var rotate = _mainCamera.transform.rotation;

		switch (_cameraTransformID)
		{
			case 1:
				pos = _oldCameraPosition;
				rotate = _oldCameraRotation;
				break;
			case -1:
				pos = _newCameraTransform.position;
				rotate = _newCameraTransform.rotation;
				break;
		}

		_mainCamera.transform.position = pos;
		_mainCamera.transform.rotation = rotate;
		
		UpdateLabels();
	}
}