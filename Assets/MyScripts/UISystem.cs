﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISystem : SingletonMonoBehaviour<UISystem>
{
    [SerializeField] GameObject _gameOverUI;
    [SerializeField] GameObject _gameClearUI;
    [SerializeField] UILabel _currentBallText;
    [SerializeField] UILabel _enemiesText;

    void Start()
    {
        GameSystem.Instance.onGameOver += OnGameOver;
        GameSystem.Instance.onGameClear += OnGameClear;
    }

    void OnGameClear(Action callback)
    {
        _gameClearUI.SetActive(true);
        callback?.Invoke();
    }

    internal void UpdateEnemies(int enemies)
    {
        _enemiesText.text = $"Enemies: {enemies}/{Constants.TotalEnemies}";
    }

    void OnGameOver(Action callback)
    {
        _gameOverUI.SetActive(true);
        callback?.Invoke();
    }

    internal void UpdateCurrentBalls(int currentBalls)
    {
        _currentBallText.text = $"Balls: {currentBalls}/{Constants.MaxBalls}";
    }
}
