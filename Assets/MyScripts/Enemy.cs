﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using static GameSystem;
using Random = UnityEngine.Random;

public class Enemy : MonoBehaviour
{
	enum MovingState
	{
		Start,
		Flipper,
		None
	}

	public event Action<Enemy> onDead;

	[SerializeField] Animator _animator;
	bool _isDead;
	float _moveSpeed = Constants.EnemyMoveSpeed;
	Vector3 _currentMoveTarget;

	static readonly int runKey = Animator.StringToHash("Run");
	static readonly int walkKey = Animator.StringToHash("Walk");
	MovingState _movingState;

	void Start()
	{
		GameSystem.Instance.onGameOver += OnGameOver;
	}

	void Update()
	{
		CheckMovingState();

		if (_movingState != MovingState.None && _isDead == false)
		{
			var lookPoint = _currentMoveTarget;
			lookPoint.y = transform.position.y;

			transform.LookAt(lookPoint);
			transform.position += transform.forward * Time.deltaTime * Constants.EnemyMoveSpeed;
		}
	}

	void CheckMovingState()
	{
		switch (_movingState)
		{
			case MovingState.Start:
				if (CheckCloseMovingTarget())
				{
					_currentMoveTarget = GameSystem.Instance.GetBottomPoint();
					_movingState = MovingState.Flipper;
				}

				break;

			case MovingState.Flipper:
				if (CheckCloseMovingTarget())
				{
					SetAnimIdle();
					_movingState = MovingState.None;
				}

				break;
		}
	}

	bool CheckCloseMovingTarget()
	{
		return Vector3.Distance(transform.position, _currentMoveTarget) < 0.3f;
	}

	void OnGameOver(Action obj)
	{
		SetAnimIdle();
		_movingState = MovingState.None;
	}

	void SetAnimIdle()
	{
		_animator.SetBool(runKey, false);
		_animator.SetBool(walkKey, false);
	}

	public void SetAnimRun()
	{
		_animator.SetBool(runKey, true);
	}

	public void SetAnimWalk()
	{
		_animator.SetBool(walkKey, true);
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag(TagName.BALL))
		{
			if (_isDead == false && GameSystem.Instance.state == GameState.Playing)
			{
				_isDead = true;
				OnDead();
			}
		}

		if (other.gameObject.name.Contains("GameOverTrigger"))
		{
			GameSystem.Instance.GameOver();
		}
	}

	void OnDead()
	{
		_animator.enabled = false;
		StartCoroutine(this.DelayMethod(1f, () => gameObject.SetActive(false)));
		onDead?.Invoke(this);
		// StartCoroutine(DoSink());
		DOTween.Kill(this);
	}

	IEnumerator DoSink()
	{
		yield return new WaitForSeconds(1f);

		foreach (Transform item in transform)
		{
			var collider = item.GetComponent<Collider>();
			var rb = item.GetComponent<Rigidbody>();

			if (collider != null)
				collider.enabled = false;
			if (rb != null)
				rb.isKinematic = true;
		}

		yield return transform.DOMoveY(transform.position.y - 5, 2.5f);
	}

	public void Init(Vector3 position)
	{
		// var pos = transform.position;
		// pos.x = position.x;
		// transform.position = pos;
		position.x += Random.Range(-1.0f, 1.0f);
		position.z += Random.Range(-1.0f, 1.0f);
		WalkToStartPosition(position);
	}

	void WalkToStartPosition(Vector3 position)
	{
		SetAnimWalk();
		_currentMoveTarget = position;
		_movingState = MovingState.Start;
	}
}