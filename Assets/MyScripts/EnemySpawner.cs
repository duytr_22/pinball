﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static GameSystem;
using Random = UnityEngine.Random;

public class EnemySpawner : MonoBehaviour
{
	public event Action onAllEnemyCleared;

	[SerializeField] GameObject _enemyPrefab;
	float _spawnDelay = Constants.SpawnDelay;
	List<GameObject> _enemiesList;

	void Awake()
	{
		_enemiesList = new List<GameObject>();
	}

	void Start()
	{
	}

	void OnDisable()
	{
		onAllEnemyCleared = null;
	}

	public void SpawnEnemies()
	{
		StartCoroutine(IESpawn());
	}

	IEnumerator IESpawn()
	{
		var count = 0;
		var total = Constants.TotalEnemies;
		var groundPosition = GameSystem.Instance.GetGroundPosition();
		while (count < total && GameSystem.Instance.state == GameState.Playing)
		{
			count++;

			var obj = Instantiate(_enemyPrefab,
				new Vector3(transform.position.x, groundPosition.y, transform.position.z), transform.rotation,
				transform);
			var position = GridSystem.Instance.GetWorldPositionByRandomIndex();
			obj.GetComponent<Enemy>().Init(position);
			obj.GetComponent<Enemy>().onDead += OnEnemyDead;
			_enemiesList.Add(obj);
			UISystem.Instance.UpdateEnemies(_enemiesList.Count);

			yield return new WaitForSeconds(_spawnDelay);
		}
	}

	void OnEnemyDead(Enemy enemy)
	{
		_enemiesList.Remove(enemy.gameObject);
		UISystem.Instance.UpdateEnemies(_enemiesList.Count);
		if (_enemiesList.Count == 0)
			onAllEnemyCleared?.Invoke();
	}
}