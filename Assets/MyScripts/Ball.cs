﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static GameSystem;

public class Ball : MonoBehaviour
{
    Vector3 _startPosition;
    Rigidbody _rigidBody;

    void Awake()
    {
        _rigidBody = GetComponent<Rigidbody>();
    }

    void Start()
    {
        _startPosition = transform.position;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.name.Contains("BallDropTrigger") && GameSystem.Instance.state == GameState.Playing)
        {
            if (GameSystem.Instance.CheckAllowBallRespawn())
                Respawn();
        }
    }

    void Respawn()
    {
        _rigidBody.velocity = Vector3.zero;
        _rigidBody.rotation = Quaternion.identity;
        transform.position = _startPosition;
    }
}
