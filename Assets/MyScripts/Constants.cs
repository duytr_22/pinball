public static class Constants
{
	public static int MaxBalls
	{
		get => _maxBalls;
		set
		{
			if (value >= 1)
				_maxBalls = value;
		}
	}

	public static int TotalEnemies
	{
		get => _totalEnemies;
		set
		{
			if (value >= 2)
				_totalEnemies = value;
		}
	}

	public static float EnemyMoveSpeed
	{
		get => _enemyMoveSpeed;
		set
		{
			if (value >= 0.25f)
				_enemyMoveSpeed = value;
		}
	}

	public static float SpawnDelay
	{
		get => _spawnDelay;
		set
		{
			if (value >= 0.25f)
				_spawnDelay = value;
		}
	}

	static float _enemyMoveSpeed = 0.5f;
	static int _maxBalls = 5;
	static int _totalEnemies = 5;
	static float _spawnDelay = 1f;
}