﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Policy;
using UnityEngine;
using static Flipper;

public class GameSystem : SingletonMonoBehaviour<GameSystem>
{
	public enum GameState
	{
		Playing,
		Over,
		Clear
	}

	public GameState state;
	public event Action<Action> onGameOver;
	public event Action<Action> onGameClear;

	[SerializeField] Flipper _flipperLeft, _flipperRight;
	[SerializeField] EnemySpawner _enemySpawner;
	[SerializeField] Transform _bottomPoint;

	[SerializeField] Transform _groundPlatform;

	// int _currentTouchID;
	List<int> _touchesList;
	int _currentBalls;

	void Start()
	{
		_currentBalls = Constants.MaxBalls;
		_touchesList = new List<int>();
		UISystem.Instance.UpdateCurrentBalls(_currentBalls);

		StartCoroutine(this.DelayMethod(1f, () =>
		{
			_enemySpawner.onAllEnemyCleared += OnGameClear;
			// _enemySpawner.SpawnEnemies();
		}));
	}

	void OnDisable()
	{
		onGameClear = null;
		onGameOver = null;
	}

	void Update()
	{
#if UNITY_EDITOR && !UNITY_REMOTE
		CheckInputEditor();
#else
		CheckInput();
#endif
	}

	void CheckInputEditor()
	{
		if (Input.GetKeyDown(KeyCode.A))
			_flipperLeft.Rotate();
		if (Input.GetKeyUp(KeyCode.A))
			_flipperLeft.ReleaseRotate();

		if (Input.GetKeyDown(KeyCode.D))
			_flipperRight.Rotate();
		if (Input.GetKeyUp(KeyCode.D))
			_flipperRight.ReleaseRotate();
	}

	void OnGameClear()
	{
		if (state != GameState.Playing)
			return;
		state = GameState.Clear;
		onGameClear?.Invoke(() =>
		{
			Debug.Log("onGameClear");
			StartCoroutine(this.DelayMethod(2f, ReloadScene));
		});
	}

	public void ReloadScene()
	{
		SceneNavigator.Instance.LoadScene(SceneName.GAMEPLAY);
	}

	internal void GameOver()
	{
		if (state != GameState.Playing)
			return;
		state = GameState.Over;
		onGameOver?.Invoke(() =>
		{
			Debug.Log("onGameOver");
			StartCoroutine(this.DelayMethod(2f, ReloadScene));
		});
	}

	void CheckInput()
	{
		foreach (var touch in Input.touches)
		{
			FlipperSide side = CheckSide(touch.position.x);
			var touchID = touch.fingerId;
			switch (touch.phase)
			{
				case TouchPhase.Began:
					if (_touchesList.Contains(touchID) == false)
						_touchesList.Add(touchID);
					if (side == FlipperSide.Left)
						_flipperLeft.Rotate();
					else
						_flipperRight.Rotate();

					break;
				case TouchPhase.Ended:
				case TouchPhase.Canceled:
					if (_touchesList.Contains(touchID) == false)
						break;

					_touchesList.Remove(touchID);
					if (side == FlipperSide.Left)
						_flipperLeft.ReleaseRotate();
					else
						_flipperRight.ReleaseRotate();
					break;
			}
		}
	}

	internal Vector3 GetBottomPoint()
	{
		return _bottomPoint.position;
	}

	public Vector3 GetClosestFlipper(Vector3 position)
	{
		float distanceLeft = Vector3.Distance(position, _flipperLeft.transform.position);
		float distanceRight = Vector3.Distance(position, _flipperRight.transform.position);

		return distanceLeft < distanceRight ? _flipperLeft.transform.position : _flipperRight.transform.position;
	}

	FlipperSide CheckSide(float x)
	{
		if (x < Screen.width * 0.5f)
			return FlipperSide.Left;
		return FlipperSide.Right;
	}

	public bool CheckAllowBallRespawn()
	{
		UISystem.Instance.UpdateCurrentBalls(--_currentBalls);
		return _currentBalls > 0;
	}

	public Vector3 GetGroundPosition()
	{
		return _groundPlatform.position;
	}
}